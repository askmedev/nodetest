# README #

## Teste NODE.JS ##

Você deverá executar as tarefas abaixo utilizando NODE.js.

Os dados das tabelas devem conter as informações mínimas necessárias para um cadastro
real nacional. Não é necessário o desenvolvimento de interfaces visuais, basta apenas que os endpoints
possam ser consumidos pelo postman por exemplo. 

Fique a vontade para tirar dúvidas sobre o teste, através do e-mail: lcavalcante@askme.com.br *

O prazo para entrega do teste é de 4 dias úteis do seu recebimento. Caso você não consiga terminar o teste a tempo, não se preocupe e entregue o que foi feito.

### 1) Clonar este projeto e criar uma branch para sua tarefa ###
### 2) Desenvolver uma base de dados em MySQL que componha as seguintes tabelas: ###
* - Clientes
* - Endereços
### 3) Criar endpoints de entrada e saída que contemplem todas as informações que podem ser transitadas neste modelo de dados e seus relacionamentos. ###
### 4) Criar um importador automatizado de dados para essas tabelas através da leitura de uma planilha do excel, lembrando que os dados não podem ser processados em tempo de execução do cliente. ###
### 5) Enviar um PULL REQUEST para validação do seu teste e aguardar o contato da nossa equipe. ###